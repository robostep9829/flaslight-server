#![allow(non_snake_case)]

const ON_OFF: [i32; 2] = [0, 1];
fn torch(a: bool) {
    std::process::Command::new("termux-torch")
        .arg(if a { "on" } else { "off" })
        .output()
        .unwrap();
}
fn flash() {
    torch(true);
    torch(false)
}
fn brightness(a: bool, b: f32) {
    if a {
        //let mut old_value = false;
        let mut switch = ON_OFF.into_iter().cycle();
        let n_times = (b * 10.).round() as u32;
        let interval = Duration::from_micros(260000)
            .checked_div(n_times)
            .unwrap_or(Duration::from_millis(50));
        let mut next_time = Instant::now() + interval;
        for _ in 0..n_times {
            let output = switch.next().unwrap();
            let result = output == 0;
            // if old_value != (result) {
            //     flash();
            //     old_value = result;
            // }
            if result {
                flash()
            }
            std::thread::sleep(next_time - std::time::Instant::now());
            next_time += interval;
        }
        torch(false)
    } else {
        torch(false)
    }
}

use std::{
    env, net,
    thread::sleep,
    time::{Duration, Instant},
};
fn main() {
    println!("Hello, world!");
    let args: Vec<_> = env::args().collect();
    dbg!(&args);
    let s = net::UdpSocket::bind(args.get(1).unwrap_or(&"0.0.0.0:65000".to_owned()))
        .expect("Отсутствует аргумент запуска");
    let mut buf = [0; 6];
    let mut current_time = Instant::now();
    let interval = Duration::from_micros(300000);
    // for i in 0..10 {
    //     brightness(true, (i as f32) / 10.);
    //     println!("{}", (i as f32) / 10.);
    //     println!(" {:?}", Instant::now() - current_time);
    //     current_time = Instant::now();
    // }
    sleep(interval);
    loop {
        let (_, addr) = s.recv_from(&mut buf).unwrap();
        match buf[0]{
            0 => {torch(buf[1]!=0)}
            1 => {
                let float_data = f32::from_le_bytes(buf[2..=5].try_into().unwrap());
                println!("{:?}", float_data);
                if Instant::now() > (current_time + interval) {
                    brightness(true, float_data);
                    current_time = Instant::now()
                }
            }
            2 => {}
            _ => todo!()
        }
        let _ = s.send_to(&[0], addr);
    }
}
